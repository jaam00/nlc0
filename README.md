## nlc0 ##

nlc0 is a major refactoring of [nlc](https://gitlab.com/jaam00/nlc), which is an implementation of the eponymous type system for natural language. nlc0's main difference from [nlc](https://gitlab.com/jaam00/nlc) is added modularity.

The Coq files (*.v) should work out of the box. The recommended way of running them is in CoqIDE, which comes bundled w/ Coq (which you must install). In CoqIDE, Tools > Coqtop arguments should be set to `-R /path/to/your/nlc0/folder nlc0` before running the files.

If you want to edit the files, compile them after editing by issuing `coqc -R . nlc0 build/*.v` in the project's root.

### Contents ###

`main.v` : The main implementation  
`exp.v` : A different (smaller) implementation (w/ tests), experimenting w/ new types and encodings (copulas, *who*, etc.; upgraded selectional restrictions; universal supercategories)  
`t-func.v` : The optional truth-functionality module (w/ tests)  
`test.v` : Tests (of the main implementation)  
`compound.v` : A showcase of different encodings of intersection types in Coq (applications, record, product and Pi-types)  

Written and best viewed in Coq 8.9  
FreeBSD License

### Troubleshooting ###

The project's root folder contains relative symlinks to targets in the `build` subfolder. If the symlinks are broken on your system, work directly on the .v files in `build`. If you have any questions, send me an email
