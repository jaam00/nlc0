(* A fragment of NL structure in NLC, modeled and best viewed in Coq 8.9 -- FreeBSD license *)

(*-----------------CAT-s:-----------------*)

Inductive NUM := SG | PLR.  (*number*)
Inductive GN := Ml | Fm.  (*gender*)
(*argument stems: flexible, noun, proper name, pronoun:*)
Inductive ST := F | N | PN | PR | Nn (x:GN) | Pn (x:GN) | Pr (x:GN).
Notation "N+" := (Nn _) (only parsing). (*noun w/ gender info*)
Notation "PN+" := (Pn _) (only parsing).
Notation "PR+" := (Pr _) (only parsing). (*unused*)
Parameter NF S0 S1 S2: Prop.  (*nonfinite, sentence types*)
Inductive SR := Phy | Sen | Inf | Lim | sr0 (x y: SR). (*selectional restrictions*)
Inductive CA := NOM | ACC | GEN | LAT | DIR | LOC. (*case/adposition: nominative, genitive, lative, directive, locative*)
Definition ACC' := NOM. (*false accusative (so-called 0-derivation)*)
Structure ACC0 := acc0 {acc:CA}.  (*NOM/ACC'/ACC*)
Canonical Structure acc_n := acc0 ACC'. (*NOM/ACC'*)
Canonical Structure acc_a := acc0 ACC.  (*ACC*)

(*phrase architecture*)
Parameter STM: SR -> CA -> NUM -> ST -> Set.  (*stem*)
Parameter PLU: SR -> CA -> NUM -> ST -> Set.  (*PL*)
Parameter XP0: SR -> CA -> NUM -> ST -> Set.  (*PN*)
Parameter XP1: SR -> CA -> NUM -> ST -> Set.  (*a ...*)
Parameter XP2: SR -> CA -> NUM -> ST -> Set.  (*the ...*)
Parameter QU0: SR -> CA -> NUM -> ST -> Type. (*entire (SG) ...*)
Parameter QU1: SR -> CA -> NUM -> ST -> Type. (*entire (PL) ...*)
Parameter QU2: SR -> CA -> NUM -> ST -> Type. (*all, every ...*)


(*-------------------SUBTYPES†:-------------------*)

Structure CXA := cxa {ga: SR -> CA -> NUM -> ST -> Type}. (*all arg-s (for sr conv func FF*)
Canonical Structure cxa_s := cxa STM.
Canonical Structure cxa_p := cxa PLU.
Canonical Structure cxa_0 := cxa XP0.
Canonical Structure cxa_1 := cxa XP1.
Canonical Structure cxa_2 := cxa XP2.
Canonical Structure cxa_q0 := cxa QU0.
Canonical Structure cxa_q1 := cxa QU1.
Canonical Structure cxa_q2 := cxa QU2.

Structure CXD := cxd {gd: SR -> CA -> NUM -> ST -> Set}.  (*(in|out)put for ADJ, input to PL..*)
Canonical Structure cxd_s := cxd STM.
Canonical Structure cxd_p := cxd PLU.
Canonical Structure cxd_0 := cxd XP0.

Structure CXS := cxs {gs: SR -> CA -> NUM -> ST -> Type}.  (*input to a, the..*)
Canonical Structure cxs_s := cxs STM.
Canonical Structure cxs_p := cxs PLU.
Canonical Structure cxs_0 := cxs XP0.
Canonical Structure cxs_q := cxs QU0.

Structure CXP := cxp {gp: SR -> CA -> NUM -> ST -> Type}. (*input to V, F, CON..*)
Canonical Structure cxp_p := cxp PLU.
Canonical Structure cxp_0 := cxp XP0.
Canonical Structure cxp_1 := cxp XP1.
Canonical Structure cxp_2 := cxp XP2.
Canonical Structure cxp_q1 := cxp QU1.
Canonical Structure cxp_q2 := cxp QU2.

Structure CXQA := cxqa {gqa: SR -> CA -> NUM -> ST -> Type}.  (*input to all*)
Canonical Structure cxqa_p := cxqa PLU.
Canonical Structure cxqa_2 := cxqa XP2.

Structure CXQY := cxqy {gqy: SR -> CA -> NUM -> ST -> Type}.  (*input to (ever|an)y*)
Canonical Structure cxqy_s := cxqy STM.
Canonical Structure cxqy_0 := cxqy XP0.

Structure CXQT := cxqt {gqt: SR -> CA -> NUM -> ST -> Set}.  (*input to entire*)
Canonical Structure cxqt_s := cxqt STM.
Canonical Structure cxqt_p := cxqt PLU.

(* † of SR -> CA -> NUM -> ST -> Type *)


(*-------------------------------ARG-s (stems):----------------------------*)

(*--------semantically simple:------------*)

Parameter ball: STM Phy (acc acc_n) SG N. (*must use (acc acc_n) instead of NOM bec of the false acc sys*)
Parameter thought: STM Inf (acc acc_n) SG N.
Parameter hut: STM Phy (acc acc_n) SG N.

(*--------semantically complex:------------*)

(*note: the Notations below depend on this section: update them if you change it*)

(*physical-informational entities*)
Structure PhyInf := mk_PhyInf {pi: SR}.
Canonical Structure PhyInf_Phy := mk_PhyInf Phy.
Canonical Structure PhyInf_Inf := mk_PhyInf Inf.
Parameter phyinf: forall {x}, STM (pi x) (acc acc_n) SG N.
Existing Class PhyInf. Existing Instance PhyInf_Phy. (*default instance for type inference*)

Check ltac:(tryif unify Inf (pi _) then idtac "ja" else idtac "ei"). (*test/ltac ex.*) (*ei*)
Check ltac:(tryif let p := constr:(eq_refl: Inf = pi _) in exact p then idtac "ja" else idtac "ei"). (*ja*)

(*physical-informational-sentient-limbed entities*)
Structure Human := mk_H {hu: SR}.
Canonical Structure H_Phy := mk_H Phy.
Canonical Structure H_Lim := mk_H Lim.
Canonical Structure H_Sen := mk_H Sen.
Canonical Structure H_Inf := mk_H Inf.
Parameter human: forall {x}, gp cxp_0 (hu x) (acc acc_n) SG N. (*note: not used*)
Parameter john': forall {x}, gp cxp_0 (hu x) (acc acc_n) SG (Pn Ml). (*cannot use XP0 (bec of CON)*)
Parameter boy': forall {x}, gs cxs_s (hu x) (acc acc_n) SG (Nn Ml).
Parameter he': forall {x: Human}, gp cxp_2 (hu x) NOM SG (Pr Ml).
Parameter him': forall {x: Human}, gp cxp_2 (hu x) ACC SG (Pr Ml).
Existing Class Human. Existing Instance H_Phy. (*type inf: default instance*)

(*-----sr conversion func:------*)

Parameter FF: forall r {z s d x y}, ga z s d x y -> ga z r d x y.

(*------type inf (for complex arg-s):-------*)

Notation pi_brac_hook := ltac:(first [exact (@FF (pi _) _ _ _ _ _ x) | exact (@FF (sr0 (pi _) (pi _)) _ _ _ _ _ x)]).
Notation hu_brac_hook := ltac:(first [exact (@FF (hu _) _ _ _ _ _ x) | exact (@FF (sr0 (hu _) (hu _)) _ _ _ _ _ x)]).
(*must use x to hook Notation "[ x ]"*)

(*-------------------------Notations (for complex arg-s):-----------------------*)

(*our only physical-informational entity is "book"*)
Notation book := phyinf (only parsing).

(*gs or gp (input for the next str)*)
Structure GX := gx' {gx:  SR -> CA -> NUM -> ST -> Type}.
Canonical Structure gx_s x := gx' (gs x). (*gs*)
Canonical Structure gx_p x := gx' (gp x). (*gp*)

(*we need these str-s for the generality of the Notation below*)
Structure HMN := hmn' {gs_gp; human'; ca'; num'; st'; hmn: (gx gs_gp) (hu human') ca' num' st'}.
Canonical Structure hmn_jh {x} := hmn' _ x _ _ _ john'.
Canonical Structure hmn_he {x} := hmn' _ x _ _ _ he'.
Canonical Structure hmn_hm {x} := hmn' _ x _ _ _ him'.
Canonical Structure hmn_by {x} := hmn' _ x _ _ _ boy'.
Notation john := (hmn hmn_jh).
Notation he := (hmn hmn_he).
Notation boy := (hmn hmn_by).
Notation him := (hmn hmn_hm).

Module Lax.
Notation "[ x ]" := ltac:(first [exact (FF _ x) | exact x]) (at level 9).
End Lax.

Module Strict.
Notation "[ x ]" := ltac:(match x with
  | phyinf => exact pi_brac_hook
  | _ phyinf => exact pi_brac_hook
  | _ (_ phyinf) => exact pi_brac_hook
  | _ (_ (_ phyinf)) => exact pi_brac_hook
  | _ (_ (_ (_ phyinf))) => exact pi_brac_hook
  | _ (_ (_ (_ (_ phyinf)))) => exact pi_brac_hook
  | hmn _ => exact hu_brac_hook
  | _ (hmn _) => exact hu_brac_hook
  | _ (_ (hmn _)) => exact hu_brac_hook
  | _ (_ (_ (hmn _))) => exact hu_brac_hook
  | _ (_ (_ (_ (hmn _)))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (hmn _))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (hmn _)))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (_ (hmn _))))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (_ (_ (hmn _)))))))) => exact hu_brac_hook
  | ?p => exact p (*relax notation*)
  | _ => idtac "[...] fail"; fail end) (at level 9).
End Strict.

(*Lax/Strict switch (comment at most one out)*)
Import Lax. (*higher priority if both on*)
Import Strict.


(*-------------------CASE/ADPOSITION:-------------------*)

(*simple*)
Parameter in': forall {z:CXP}{s:SR}{x:NUM}{y}, gp z s NOM x y -> S0 -> S0.
Notation "'in'" := in'.

(*simple w/ subtyping*)
Definition XP2DIR {s:SR}{x:NUM}{y} := XP2 s DIR x y.
Parameter xp'd: forall {s:SR}{x:NUM}{y}, @XP2DIR s x y. (*let to(wards) land on this type, and coerce it to ..LAT..*)
Parameter towards to: forall {z:CXP}{s:SR}{x:NUM}{w y}, gp z s (acc w) x y -> @XP2DIR s x y. (*to takes ACC not NOM*)
Parameter xp'd_lat:> forall {s:SR}{x:NUM}{y}, @XP2DIR s x y -> XP2 s LAT x y.

(*poly(sem|morph)ic*)

(*generic (a Phy ver would have Phy instead of {s:SR})*)
Definition At {s x y} := ltac:(first [exact (XP2 s LAT x y) | exact (S0 -> S0)]).
Parameter at': forall {z s w x y}, gp z s (acc w) x y -> @At s x y. (*at takes ACC not NOM*)
Parameter at_lat:> forall {s x y}, @At s x y -> XP2 s LAT x y.
Definition S0S0 := S0 -> S0.
Parameter at_soso:> forall {s x y}, @At s x y -> S0S0.
Notation "'at'" := at'.
Identity Coercion ic: S0S0 >-> Funclass.


(*-------------------ADJECTIVE:-------------------*)

Definition ADJ {x d z y}(s:SR) := gd x s d z y -> gd x s d z y.
Parameter red blue: forall {x d z y}, @ADJ x d z y Phy.
Parameter good dull: forall {x d z y}, @ADJ x d z y Inf.
Parameter limbed: forall {x d z y}, @ADJ x d z y Lim.


(*-------------------ADJECTIVAL ADVERB:-------------------*)

Definition A_ADV {x d z y s} := @ADJ x d z y s -> @ADJ x d z y s.
Parameter very: forall {x d z y s}, @A_ADV x d z y s.


(*-------------------GENERIC ADVERB:-------------------*)

Structure GADV := mkGADV {gadv: Type}.
Canonical Structure c_adj {x y z w v} := mkGADV (@ADJ x y z w v).           (*adjectival..*)
Canonical Structure c_vf {k l m n o}{x:Prop} := mkGADV (gp k l m n o -> x). (*verbial/flexibial..*)
Canonical Structure c_s := mkGADV S0.                                       (*sentential adverb*)
Definition G_ADV (x:GADV) := gadv x -> gadv x.
Parameter g_adv: forall {x}, G_ADV x.
Notation madly := g_adv.


(*-------------------DETERMINER:-------------------*)

Parameter the: forall {b:CXS}{x:SR}{d:CA}{z:NUM}{y}, gs b x d z y -> gp cxp_2 x d z y.
Parameter a: forall {b:CXS}{x:SR}{d w}, gs b x d SG w -> gp cxp_1 x d SG w.
Parameter this: forall {b:CXS}{x:SR}{d:CA}{w:ST}, gs b x d SG w -> gp cxp_2 x d SG w.
Parameter these: forall {b x d w}, gs b x d PLR w -> gp cxp_2 x d PLR w. (*cannot use XP.. (bec of con (also above))*)


(*-------------------PLURAL:-------------------*)

(*note: PL sends STM and XP1 to PLU*)
Parameter PL: forall {t:CXD}{f:SR}{d:CA}{z:ST}, gd t f d SG z -> gp cxp_p f d PLR z. (*cannot use PLU (bec of con)*)
Notation "-s" := PL (only parsing).


(*-------------------QUANTIFIER:-------------------*)

Parameter all: forall {x f d z}, gqa x f d PLR z -> gp cxp_q2 f d PLR z.
Parameter every: forall {x f d z}, gqy x f d SG z -> gp cxp_q2 f d SG z.
(* Parameter entire: forall {x y f d z}, gqt y x f d z -> gp cxp_q1 x f d z. *)
Structure ENTIRE := mkENTIRE {enti:Type}.
Canonical Structure ent_sg {x z f} := mkENTIRE (gqt cxqt_s x f SG z -> gs cxs_q x f SG z). (*QU0 SG*)
Definition ent_pl' x z f := gqt cxqt_p x f PLR z -> gp cxp_q1 x f PLR z. (*fix for "redundant proj"*)
Canonical Structure ent_pl {x z f} := mkENTIRE (ent_pl' x z f). (*QU1*)
Definition ent_pl'' x z f := gqt cxqt_p x f PLR z -> gs cxs_q x f PLR z.
Canonical Structure ent_pl0 {x z f} := mkENTIRE (ent_pl'' x z f). (*QU0 PLR*)
Parameter entire': forall {x}, enti x. (*type inference for this fails (see below)*)
Notation "'entire'" := (@entire' _) (at level 200).
Notation "'entire' x" := ltac:(first [exact (@entire' ent_sg x) | exact (@entire' ent_pl x) | exact (@entire' ent_pl0 x)]) (at level 200).


(*-------------------TENSE-ASPECT-MOOD:-------------------*)

Parameter PAST PRES: forall {x:Type}, (NF -> x) -> x.


(*-------------------SENTENCE:-------------------*)

Parameter s0s1:> S0 -> S1.
Parameter s1s2:> S1 -> S2.
Notation S := S2.


(*-------------------VERB:-------------------*)

Parameter read: forall {b c:CXP}{z w:NUM}{y x f}, NF -> gp b Sen NOM z y -> gp c Inf (acc x) w f -> S0.


(*-------------------FLEXIBLE:-------------------*)

(*simple*)

Structure Sleep := mSleep {sleep':Type}.
Canonical Structure sleep_s := mSleep (gs cxs_s Inf NOM SG F). (*need gs cxs_s instead of STM; all events are Inf*)
Canonical Structure sleep_d := mSleep (gd cxd_s Inf NOM SG F). (*req-ed for type inference only*)
Canonical Structure sleep_q := mSleep (gqy cxqy_s Inf NOM SG F). (*req-ed for type inference only: every sleep*)
Canonical Structure sleep_f {b:CXP}{v:NUM}{y} := mSleep (NF -> gp b Sen NOM v y -> S0).
Parameter sleep: forall {b:Sleep}, sleep' b.

Structure Stone := mStone {stone':Type}.
Canonical Structure stone_s := mStone (gs cxs_s Phy (acc acc_n) SG F). (*need gs cxs_s instead of STM & (acc acc_n) instead of NOM*)
Canonical Structure stone_d := mStone (gd cxd_s Phy (acc acc_n) SG F). (*equiv to the above (for type inference only)*)
Canonical Structure stone_q := mStone (gqy cxqy_s Phy (acc acc_n) SG F). (*________''________*)
Canonical Structure stone_f {b c:CXP}{v u:NUM}{x f y} := mStone (NF -> gp b Lim NOM v x -> gp c Phy (acc f) u y -> S0).
Parameter stone: forall {b:Stone}, stone' b.
Existing Class Stone. Existing Instance stone_s. (*type inf: default instance*)

(*variadic*)

(*variadic func support (a generic ver would have {s:SR} instead of Phy)*)
Definition variad u v x := gp cxp_2 Phy u v x -> S0.
Parameter variad_S0:> forall u x y, variad u x y -> S0.

Structure Throw := mThrow {throw':Type}.
Canonical Structure throw_x := mThrow (gs cxs_s Inf NOM SG F).
Canonical Structure throw_d := mThrow (gd cxd_s Inf NOM SG F).
Canonical Structure throw_s {z w v x y f h}{b c:CXP} := mThrow (NF -> gp b Lim NOM z x -> gp c Phy (acc h) w y -> variad LAT v f).
Parameter throw: forall {b:Throw}, throw' b.

Structure Walk := mWalk {walk':Type}.
Canonical Structure walk_x := mWalk (gs cxs_s Inf NOM SG F).
Canonical Structure walk_d := mWalk (gd cxd_s Inf NOM SG F).
Canonical Structure walk_s {b:CXP}{z v x y} := mWalk (NF -> gp b Lim NOM z x -> variad DIR v y).
Parameter walk: forall {b:Walk}, walk' b.


(*-------------------SENTENTIAL ADVERB:-------------------*)

Parameter however: S0 -> S1.
Parameter importantly crucially: S1 -> S2.


(*-------------------CON:-------------------*)

(*default terms*)
Parameter ca:CA.
(* Parameter sr:SR. *)
Parameter num:NUM.
Parameter st:ST.
Parameter cxd':CXD.

(*con type inf: default instances*)
Existing Class CA. Existing Instance ca.
Existing Class NUM. Existing Instance num.
Existing Class ST. Existing Instance st.
Existing Class CXD. Existing Instance cxd'.
Existing Class SR. Existing Instance Phy.

Scheme Equality for CA. (*for CA_beq*)
Scheme Equality for SR. (*for SR_beq*)

(*---sentential con:---*)

Parameter s_base: S0 -> S0 -> S0. (*cannot use S*)

(*---nominal con:---*)

Parameter x_base: forall {b c s t k l o p x y}, gp b s k o x -> gp c t l p y ->
(if CA_beq k l then (if SR_beq s t then gp cxp_2 s k num st else gp cxp_2 Phy k num st)
else (if SR_beq s t then gp cxp_2 s ca num st else gp cxp_2 Phy ca num st)).

(*---adjectival con:---*)

Parameter aa: forall {b c s t k l o p x y e f g h i}, @ADJ b s k x o -> @ADJ c t l y p -> @ADJ e f g h i.
Parameter adj_base: forall {b c s t k l o p x y e f g h}, @ADJ b s k x o -> @ADJ c t l y p ->
(if SR_beq o p then @ADJ e f g h p else @ADJ e f g h (sr0 o p)).

(*---adverbial con:---*)

Parameter gadv_base: forall x, G_ADV x -> G_ADV x -> G_ADV x.

(*---all con-s:---*)

Structure CON := mCON {dom_con:Type; ran_con:Type; con: dom_con -> ran_con}.
Canonical Structure con_s := mCON _ _ s_base. (*S0*)
Canonical Structure con_variad {x} := mCON (variad x _ _) _ s_base. (*variad-as-S0*)
Definition x_fix {c s t k l p y} := gp c t l p y -> (*fix for "redundant proj" warning*)
(if CA_beq k l then (if SR_beq s t then gp cxp_2 s k num st else gp cxp_2 Phy k num st)
else (if SR_beq s t then gp cxp_2 s ca num st else gp cxp_2 Phy ca num st)).
Canonical Structure con_x {b c s t k l o p x y} := mCON _ x_fix (@x_base b c s t k l o p x y). (*gp..*)
Definition a_fix {c t l y p o e f g h} := @ADJ c t l y p -> (if SR_beq o p then @ADJ e f g h p else @ADJ e f g h (sr0 o p)).
Canonical Structure con_a {b c s t k l o p x y e f g h} := mCON _ a_fix (@adj_base b c s t k l o p x y e f g h). (*ADJ*)
Definition g_fix {x} := G_ADV x -> G_ADV x.
Canonical Structure con_ga {x} := mCON _ g_fix (gadv_base x). (*G_ADV*)
Notation "'and'" := (con _) (at level 8). (*level req-ed (all not-s of "and" must be on the same level)*)
Notation "'and' x y" := (con _ x y) (at level 8, x, y at level 9).

(*---ad(j|v) con:---*)

Ltac adj_not x y z := (refine ((@aa _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x y) z)). (*need refine; using adj_base fails*)

Notation and_abs x y z := ltac:(lazymatch type of x with
  | G_ADV _ => lazymatch type of y with (*adv*)
      | G_ADV _ => refine (gadv_base _ (@g_adv _) (@g_adv _) z) end
  | ADJ ?k => lazymatch type of y with (*adj*)
      | ADJ ?l => let u := constr:(z: ga _ _ _ _ _) in lazymatch type of u with (*should i use gs?*)
        | ga _ ?m _ _ _ => lazymatch z with
          | @FF _ _ _ _ _ _ _ => adj_not x y ([z]: ga _ l _ _ _)
          (*must match 2nd arg-s sr (per con_a); using @F(F|gp) and ga _ _.. fails*)
          | _ => tryif unify k m then (tryif unify l m then adj_not x y z
            else idtac "err adj"; fail) else idtac "err adj0"; fail
        | _ => idtac "err adj1"; fail end end end
  | _ => idtac "uncaught"; fail end).

Notation "'and' x y z" := (and_abs x y z) (at level 8, x, y, z at level 9).

(* l6pp *)