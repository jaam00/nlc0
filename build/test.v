Require Import main.

(*Lax/Strict switch (comment at most one out)*)
Import Lax. (*higher priority if both on*)
Import Strict.

(*------------TESTS:-------------*)

(*The main tests: the following examples are generously commented and should be self-explanatory*)

Check PAST throw john. (*"John threw" type checks..*)
Fail Check PAST throw john: S. (*..but not as a sentence.*)
Check PAST throw john (-s stone): S.
Check PAST throw john (-s stone) (at (the hut)): S.
(*"At the hut" can be the 3rd argument of "throw" (above)..*)
Fail Check PAST throw john (at (the hut)). (*..but not the 2nd one.*)
Fail Check PAST throw (a hut). (*"A hut" cannot be the 1st argument of "throw"..*)
Check PAST throw [a hut]. (*..except in brackets (this works in Lax mode only).*)
Fail Check PAST throw john (-s stone) (in (the hut)).
(*"In the hut" cannot be an argument of "throw"..*)
Check in (the hut) (PAST throw john (-s stone)): S. (*..but it can be a sentence modifier..*)
Check at (every hut) (PAST throw john (-s stone)): S. (*..and so can "at every hut"..*)
Check however (PAST throw john (-s stone)): S. (*..and sentential adverbs like "however".*)
Fail Check and (PAST throw john (a stone)) john.
(*Connectives cannot range over a sentential and nominal argument..*)
Check and (every john) (all (the (-s boy))). (*..but can range over nominal..*)
Check and (PAST walk (-s boy) (to (all (the (-s hut))))) (PAST sleep john). (*..or sentential arguments..*)
Check and (PAST walk (-s boy) (to (all (-s hut)))) (PAST sleep john). (*..or sentential arguments..*)
Check and (PAST walk john) (PAST sleep john). (*..(also w/ optional arguments omitted).*)
Check PAST walk [and (every john) (all (the (-s boy)))] (to (the hut)).
(*every john and all the boys walked to the hut*)
Fail Check in (all (the hut)). (*ungrammatical?*)
Check in (the (entire hut)).
Check madly ((in (the (entire hut))) ((PAST walk) (all (the (-s boy))))): S.
(*all the boys walked madly in the entire hut*)

(*Examples of intersection types:*)
Check john: XP0 (hu _) NOM SG (Pn Ml).
Check john: XP0 (hu H_Phy) NOM SG (Pn Ml).
Check john: XP0 Phy NOM SG (Pn Ml).
Check limbed john: XP0 Lim NOM SG PN+.
Fail Check limbed john: XP0 Phy NOM SG (Pn Ml).
Fail Check limbed (john: XP0 Phy NOM SG (Pn Ml)).
(*"John" is an XP, human, proper name, male, in nominative, singular, a physical entity..*)
Check john: XP0 (hu H_Lim) NOM SG PN+. (*..and a limbed entity.*)
Check john: XP0 Lim NOM SG (Pn Ml). (*The normalized version of the previous type.*)
Check the john: XP2 _ NOM SG (Pn Ml). (*"The John" is another type of XP.*)
Check and (a (good throw)) (entire (-s throw)).
(*"Throw" is a flexible (a function or argument (as above))..*)
Check and (PRES stone john (a boy)) (PAST throw john (-s stone) ((at (-s stone)))).
(*..and "stone" likewise.*)
(*So "at stones" is an XP, flexible in lative, plural and a physical entity:*)
Check at (-s stone): XP2 Phy LAT PLR F.
(*As prescribed by "red", "red John" is a physical entity:*)
Check red john: XP0 Phy NOM SG (Pn Ml).
Check [red john]: XP0 Lim NOM SG PN+. (*"PN+" means proper name w/ gender info*)
(*..but we can make it into a limbed one w/ our bracket notation (above).*)
Check hut: STM Phy _ _ _. (*"Hut" is a stem and always a physical entity..*)
Check [hut]: STM Lim _ _ _. (*..so we can make it into a limbed one only in Lax mode..*)
(*..(the above line fails in Strict mode).*)
Check and (the (entire hut)) (all (-s john)): XP2 Phy NOM _ _. (*"All Johns and the entire hut" is an XP and physical entity in nominative..*)
Check and (the (entire hut)) (all (-s john)): XP2 Phy ACC' _ _. (*..or pseudo-accusative (by zero-derivation)..*)
Check [and (the (entire hut)) (all (-s john))]: XP2 Sen ACC' _ _. (*..which can be made into a sentient entity in Lax mode only*)

(*"John threw madly blue stones at the hut and red limbed boys." has 2 parses:*)
Check madly (PAST throw) john (blue (-s stone)) (at (and (the hut) (red [limbed (-s boy)]))): S.
Check PAST throw john (madly blue (-s stone)) (at (and (the hut) (red [limbed (-s boy)]))): S.
(*Here we used "[...]" to make a limbed entity into a physical one.*)
(*Arguments of verbs and flexibles must be proper XPs:*)
Fail Check PAST throw (the boy) (entire stone) (to him).
Check PAST throw (the boy) (the (entire stone)) (to him): S.
(*Arguments of V and F must have correct cases and selectional restrictions:*)
Check PAST throw he (all (-s stone)) (at john): S.
Check PAST throw john (all (the (-s stone))) (at him): S.
Check PRES throw (all (the (-s boy))) (every ball) (to him): S.
Fail Check PAST throw john (all (-s stone)) (at he). (*wrong case*)
Fail Check PAST throw him (all (-s stone)) (at john). (*wrong case*)
Fail Check PRES throw john (a walk). (*wrong case and selectional restriction*)
Fail Check PRES throw john [a walk]. (*wrong case?*)
Fail Check [a walk]: _ _ (acc _) _ _.
Check [a walk]: _ _ ACC' _ _.
Fail Check PRES throw (a walk) john. (*wrong selectional restriction*)
Check PRES throw [a walk] john. (*succeeds in Lax mode only*)
Check PRES throw john (-s stone) (at (all (madly (madly red) (red (-s hut))))): S.
Check PRES throw john (-s stone) (at (all (madly (madly red) (red [limbed [-s hut]])))): S.
(*We can stack adverbs and adjectives (note that the above line fails in Strict mode)..*)
Fail Check PRES throw john (-s stone) (at (madly madly red (red [limbed [-s hut]]))).
Fail Check PRES throw john (-s stone) (at (madly (madly (red (red [limbed [-s hut]]))))).
(*..but must be careful w/ parentheses.*)

Check all ((and madly madly) red (red (red (and blue red (-s john))))): QU2 _ _ _ _.
Check all ((and madly madly) red (red (red [and blue limbed [-s john]]))).
Check all ((and madly madly) red (red [and blue limbed [-s john]])).
(*Adjectival and adverbial connectives are supported..*)
Fail Check all (and madly madly red (red (red [and blue limbed [-s john]]))).
(*..but again we must be careful w/ parentheses.*)
Check red [limbed john].

(*Tests for individual categories*)

(*semantically complex arg-s*)
Check boy': STM _ ACC' SG _.
Fail Check boy': STM _ ACC SG _.
Fail Check him': XP2 _ NOM SG _.
Fail Check he': XP2 _ ACC SG _.
Fail Check john': XP0 _ ACC' SG PN.
Check john': XP0 _ ACC' SG PN+.

(*notations for semantically complex arg-s*)
Compute gx (gx_p cxp_0).
Compute gx (gx_s _).
Compute hmn hmn_he: XP2 Sen _ _ _.
Compute hmn hmn_jh: XP0 Sen _ _ _.
Compute he: XP2 Sen _ _ _.
Compute him: XP2 Sen _ _ _.
Compute john: XP0 Sen _ _ _.
Compute [hmn hmn_he]: XP2 Sen _ _ _.
Compute [hmn hmn_jh]: XP0 Inf _ _ _.
Compute [he]: XP2 Sen _ _ _.
Compute [boy]: STM Lim _ _ _.
Check book: STM Inf NOM SG N.
Check book: STM Phy ACC' SG N.
Fail Check book: STM Lim NOM SG N.
Check john: XP0 Lim NOM SG (Pn Ml).
Compute [john]: XP0 Lim NOM SG (Pn Ml).
Check FF _ john: XP0 Lim NOM SG (Pn Ml).
Check boy: STM _ _ _  N+.
Fail Check book: STM _ _ _ N+.
Check [book].
Check [hut].

(* case/adposition *)
Check in' john.
Fail Check in' ball.
Check in' john ((in' john) _:S0): S0.
Check to _: XP2 _ DIR _ _.
Check to _: XP2 _ LAT _ _.
Check at john: XP2 _ LAT _ _.
Check at john: S0 -> S0.
Fail Check at john: S0.
Check at john (_:S0).
Check at john ((at john) _:S0): S0. (*bec there's no anaphora res*)
Check in john ((in john) _:S0): S0.
Fail Check in book.

(* adjective *)
Fail Check good him.
Fail Check good he.
Check good john.
Check red: ADJ Phy: Type.
Check [red book]: STM _ ACC' _ _.
Check red book: STM _ NOM _ _.
Check good: ADJ Inf.
Check good book.
Check [good book].
Check red [good book].
Check [red [good book]].
Check good [red [good book]].
Check [book].
Check [good [good book]].
Check good [book].
Check limbed: ADJ Lim.
Fail Check red red.
Check red (red _).
Fail Check limbed book.
Fail Check [limbed book].
Check blue (red john).
Check blue (red book).
Check red [limbed john].
Fail Check red (limbed john).
Compute red (red john).
Check red (red (red book)).
Compute red book.
Fail Check limbed (red john).
Fail Check limbed (red book).
Check [limbed [red book]].
Check [limbed [red [book]]].
Check limbed (FF _ (red book)).
Check [red hut].
Check good boy: gs _ _ _ _ _.
Fail Check [red] [hut]. (*parsing*)
Fail Check red [limbed book].
Check red [limbed [book]].
Check limbed [red john].
Check red book.
Check [red book].
Check red john.
Check blue (red john).
Check FF _ (limbed (FF _ hut)).
Check good [red john].
Check good [red [limbed john]]. 
Fail Check limbed (red [limbed john]).
Check to john.
Check to (red john).
Check good john: XP0 _ _ _ PN+.

(* adjectival adverb *)
Check very (very red) [limbed [blue john]].
Fail Check very (very red) limbed.
Fail Check very (very red) [limbed _].

(* generic adverb *)
Fail Check madly madly.
Fail Check madly hut.
Fail Check madly john.
Check madly (madly red) [limbed [john]].
Check madly (madly red) (red [limbed [john]]).
Check very (very (madly red)) [limbed [blue john]].
Check madly (very (very (madly red))) [limbed [blue john]].
Check red (red book).
Check red (red (book)).
Fail Check red (good book).
Check good [red [good book]].
Fail Check good book: _ Phy _ _.
Fail Check red book: _ Inf _ _.
Check good (good book).
Check blue [good [red book]].
Check red [good book].
Check to (madly (very (very (madly red))) [limbed [blue john]]).
Fail Check madly (very (very (madly red))) (to [limbed [blue john]]).
Check madly (very (very (madly red))) [good [blue book]].

(* determiner *)
Fail Check the the.
Check the book.
Fail Check the (the book).
Fail Check a (the book).
Check the ((madly red) [limbed [good john]]).
Fail Check these john.
Fail Check these book.
Fail Check red the.
Fail Check madly a.
Fail Check red (a hut).
Fail Check madly the.
Check a (very (very good) [red book]).
Check to (a book): _ Phy _ _.
Check to (the john).
Check to john.
Fail Check to (to john).
Fail Check the (to john).
Check to (the (very (very good) [red book])).
Check at (the (red book)).
Check the (good book): XP2 _ NOM _ _.
Check at (the (good book)).
Check [[the (good book)]].
Check [the (good book)].
Check at (the book).
Check at (the thought).
Check at (the (good [red book])).
Check at (the (good book)).
Check @at' _ _ _ _ _ (the (good book)).
Check at [the (good book)]: _ -> _.
Check at [the (good book)]: XP2 _ _ _ _.
Check very red.
Fail Check a (very red).
Check a (red book).
Check [a (red book)].

(* plural *)
Check PL book.
Check PL book. Check PL(book).
Check -s book.
Check -s john.
Check these (-s john).
Check a john.
Check a book.
Fail Check a (-s john).
Check the ((madly (madly red)) [limbed [blue (-s john)]]).
Check the (very (madly (madly red)) [limbed [blue (-s john)]]).
Check XP1 _ _ _ _: Set.
Check STM _ _ _ _: Set.
Check to (the ((madly (madly red)) [limbed [blue (-s john)]])).
Check to (the ((madly (madly red)) (blue (-s book)))).
Check in (the (-s book)).
Check in [the (good [-s book])]: _ -> _.

(* quantifier *)
Fail Check entire' hut.
Check entire hut.
Check entire: _ -> _.
Check (_: enti ent_sg) hut.
Check (_: enti ent_pl) (-s hut).
Check entire: _ -> _.
Check (entire: _ -> _) hut.
Check entire hut: gs _ _ _ _ _.
Check entire (-s hut): gp _ _ _ _ _.
Check every book.
Fail Check the (every book).
Fail Check a (every book).
Fail Check all (the book).
Check the (entire book).
Fail Check PL (entire book).
Fail Check entire john.
Fail Check every (a book).
Fail Check all (a book).
Check all (-s book).
Fail Check all book.
Fail Check all john.
Fail Check all (a book).
Fail Check all the.
Check all (the _).
Check all (the (red (-s book))).
Check all (very (very red) (blue (-s book))).
Fail Check all (all (red (blue (-s book)))).
Check in (all (very (very red) (blue (-s book)))).
Check in (all (the (-s hut))).
Fail Check in (all (the hut)).
Check in (every hut).
Check in (the (entire hut)).
Fail Check good (entire book).
Check the (entire (madly good book)).
Check a (entire book).
Check the (entire book).
Check these ((entire (-s thought)): gs _ _ _ _ _).
Check the (entire (-s ball)).
Check these (_ : gs _ _ _ _ _).
Check entire (-s thought): gs _ _ _ _ _.
Check entire (-s hut).
Check every ball.

(* verb *)
Check PAST read [a (good john)] [a (red book)].
Check PAST read [a (good john)] _.

(* flexible *)

(* simple *)
Check sleep: NF -> _.
Check sleep: gs _ _ _ _ _.
Check @PAST: forall {x:Type}, (NF -> x) -> x.
Check john: gp _ _ _ _ _.
Check PAST sleep john: S.
Fail Check PAST sleep john: Prop.
Check a sleep: XP1 Inf NOM SG F.
Fail Check sleep: STM Inf NOM SG F.
Check good sleep.
Check the (madly good sleep).
Check PAST sleep [a (good john)].
Check PAST sleep [red (-s john)].
Check madly (madly (PAST sleep)) [madly (madly red) [limbed [blue (-s john)]]]: S0.
Check PAST sleep [madly (madly red) [limbed john]]: S0.
Check PAST sleep [madly (madly red) [limbed [blue (-s john)]]].
Check in (blue [good [-s book]]) (PAST sleep [madly (madly red) [limbed [blue (-s john)]]]).
Check at [the (good book)] ((PAST sleep) john).
Fail Check PAST sleep [to john].
Fail Check PAST sleep (a hut).
Check every sleep.
Check the (entire stone). (*requires default instance*)
Check the (blue stone).
Check PAST stone [a (good john)] (-s john): S0.
Check PAST stone [red (-s john)].
Check madly (madly (PAST stone)) [madly (madly red) [limbed [blue (-s john)]]] (the (-s hut)).
Check PAST stone [madly (madly red) [limbed john]] [good [-s hut]]: S0.
Check PAST stone [madly (madly red) [limbed [blue (-s john)]]].
Check in (blue [good [-s hut]]) (PAST stone [madly (madly red) [limbed [blue (-s john)]]] (-s book)).
Fail Check at [the (good book)] ((PAST stone) john).
Fail Check PAST stone [to john].
Fail Check PAST stone (a hut).
Fail Check good stone.
Check limbed [hut].
Check limbed [stone].
Check good [hut].
Fail Check good stone.
Compute good [stone]. (*requires default instance*)
Compute good [@stone stone_s].
Check red stone.
Fail Check limbed (red stone).
Check limbed [red stone].
Check red stone: _ Phy _ _ _.
Fail Check red stone: _ Inf _ _ _.
Check blue (red stone).
Check in (the stone).
Fail Check good stone.
Check red (red stone).
Check to (the stone).
Check these (-s stone).
Check every stone.
Check entire stone.
Check all (-s stone).
Fail Check all stone. (*interpreted as count noun only (mass/count distinction not implemented)*)

(* variadic *)
Check a (good throw).
Check PAST throw john (a book): S0.
Check PAST throw john him: S0.
Check PAST throw john (a book).
Check PAST throw john (a (entire book): gp _ _ _ _ _) (at john): S0.
Check PAST throw john (a book) (at john): S0.
Check PAST throw john (a book) (towards john): S0.
Check PAST throw john (a ball) (to john): S0.
Check PRES throw john (the (entire hut): gp _ _ _ _ _).
Check PRES throw john (-s stone).
Check PRES throw john (a stone) (at (a ((madly red) [good john]))).
Check PRES throw john (a stone): _ -> _.
Fail Check PRES throw john (a stone) (at (the (good book))).
Check PRES throw john (a stone) [at (the (good book))].
Check PRES throw john (a ball) [at (the (good book))].
Check PAST throw john (a book) (at (red (-s stone))): S0.
Check PRES throw [madly (madly red) [limbed john]] (a book).
Check PRES throw [madly (madly red) [limbed john]] (a ball).
Check PRES throw [madly (madly red) [limbed john]] him.
Check PRES throw [madly (madly red) [limbed john]] (a ((madly red) (blue [good book]))).
Check PRES throw [madly (madly red) [limbed [-s john]]].
Check PRES throw [madly (madly red) [limbed [-s john]]] (a ((madly red) (blue [good book]))).
Fail Check PRES throw [to john] (a book).
Fail Check PRES throw john (to (a book)).
Check madly (madly (PRES throw)) [the (madly (madly red) [limbed [good [-s john]]])] (all (these (red (-s book)))).
Check PAST walk john.
Check PAST walk john: S0.
Check PAST walk john _: S0.
Check PRES walk john (towards (-s book)): S0.
Check a (good walk).
Check dull (-s walk).

(* sentential adverb *)
Check however (PRES throw john (a stone) (at (a ((madly red) [good book])))): S.
Check importantly (however (PRES throw john (a ball) (to (the (very (madly (madly red)) [limbed [blue (-s john)]]))))): S.
Check importantly (however (PAST read [the (very (madly (madly red)) [limbed [blue (-s john)]])] [a ((madly red) (blue [good book]))])): S.
Check PAST read (all (-s john)) [a book].
Check PRES throw john (a book): S.
Check however (in (-s hut) (PAST walk john (towards (the (-s book))))): S.
Check crucially (in (the hut) (PAST walk john)): S.
Fail Check crucially (in (the hut) ((PAST walk john) (in (a hut)))): S.
Check crucially (in (the hut) ((in (a hut)) ((PAST walk john)))): S. (*Crucially, john walked in a hut in the hut.*)
Check crucially (in (the hut) (madly (PAST walk john) (to (a hut)))): S. (*Crucially, john walked madly to a hut in the hut.*)
Check crucially (in (the hut) (madly (PAST walk) john (to (a hut)))): S. (*Crucially, john walked madly to a hut in the hut.*)
Check crucially (in (all (the (-s hut))) (madly (PAST walk) john (to (a hut)))): S. (*Crucially, john walked madly to a hut in all the huts.*)
Check crucially (madly (in (the hut) (madly (PAST walk john) (to (a hut))))). (*Crucially, madly, john walked madly to a hut in the hut.*)
Fail Check madly (crucially (in (the hut) (madly (PAST walk john) (to (a hut))))). (*Madly, crucially, john walked madly to a hut in the hut.*)
Check however (PRES throw john (a stone) (at john)): S.
Check importantly (PRES throw john (a stone) _): S.
Fail Check importantly (importantly (PRES throw john (-s stone))).
Fail Check however (importantly (PRES throw john (a ball))).
Fail Check however (however (PRES throw john (a stone))).

(* connective *)

(* (mostly) argumental connective*)
Check con _ madly madly.
Check con _ red red.
Check con _ (PAST sleep john) (PAST sleep john). (*checks bec we set S0 instead of S*)
Check con _ (a book) (a book). (*checks bec we generalized from (PLU|XP(1|2)) to gp*)
Check con _ (john:gp _ _ _ _ _) john.
Fail Check con _ [john] john.
Check con _ john john.
Check con _ (a book) john.
Check con _ john john.
Check con con_x (-s book) (a stone).
Check con con_x (a sleep) (-s sleep).
Check con con_a _ _.
Check con con_a _ _.
Check and.
Fail Check and _ _.
Check and john (a book).
Check and red limbed.
Check madly (and red limbed).
Fail Check and _ _.
Check madly (and red limbed).
Check madly (con (_:CON) red limbed).
Check madly (con (mCON _ _ _) red limbed).
Fail Check and john (PAST sleep john).
Check (and madly madly) red.
Check and john john.
Fail Check and [john] john.
Check and (a book) john.
Check and (john:gp _ _ _ _ _) john.
Fail Check and (john:XP1 _ _ _ _) john.
Check and (john:gp _ _ _ _ _) john.
Check and (a hut:gp _ _ _ _ _) (a hut).
Check and (-s hut) (-s hut).
Check and john (-s ball).
Check and john john.
Check and (a hut:gp _ _ _ _ _) (a hut).
Check and red limbed.
Fail Check and red (_:S).
Fail Check and (_:S) (_:ga _ _ _ _ _).
Fail Check and john (PAST sleep john).
Fail Check and hut (PAST sleep john).

(* (mostly) adjectival/adverbial connective *)
Check @g_adv c_adj (con _ red red) john.
Check @g_adv c_adj (con _ red limbed) [john].
Check @g_adv c_adj (_ _ red limbed) john.
Check gadv_base c_adj _ _ (gadv_base _ _ _ (con _ red red)) hut.
Check gadv_base c_adj _ _ (gadv_base _ _ _ (con _ red limbed)) [hut].
Check gadv_base c_adj _ _ (gadv_base _ _ _ (_ _ red limbed)) hut.
Check @g_adv c_adj (_ _ red limbed) hut.
Check @g_adv c_adj _ john.
Check @g_adv c_adj _ john.
Check @g_adv c_adj _ hut.
Check gadv_base c_adj g_adv g_adv ((gadv_base c_adj g_adv g_adv) (con _ red red)) john.
Check gadv_base c_adj g_adv g_adv _ john.
Check gadv_base c_adj _ _ _ john.
Check gadv_base c_adj _ _ _ hut.
Check gadv_base c_adj _ _ (_ (con _ red red)) hut.
Check gadv_base c_adj _ _ (_ (con _ red limbed)) hut.
Check ltac:(refine ((@aa _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) hut)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) [hut])).
Fail Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) hut)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) [hut])).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) [john])).
Fail Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) john)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) [john])).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red red) hut)).
Check madly (con (_:CON) red limbed).
Check madly (con (mCON _ _ _) red limbed).
Check limbed [red hut].
Check (and red red) _.
Check (and madly madly) limbed john.
Check (and madly madly) limbed [red hut].
Check (and madly madly) limbed [red john].
Check and_abs red limbed [john].
Check and red limbed [john].
Check (and red red) ((and red red) _).
Check red (red _).
Check and red limbed.
Check (and red limbed) [book].
Check madly red [and red good [book]].
Check madly red [(and red good) [book]].
Check (and madly madly) (and red red) hut.
Check madly ((and madly madly) (and red red)) hut.
Check madly ((and madly madly) (madly (and red red))) hut.
Check madly ((and madly madly) (madly red)) ((and red red) (red hut)).
Check madly ((and madly madly) ((and madly madly) (madly (madly red)))) (red ((and red red) ((and red red) (red hut)))).
Check madly ((and madly madly) (madly (and limbed red))) [hut].
Check (and madly madly) (and red red) ((and red red) hut).
Fail Check (and madly madly) (and red limbed) hut.
Check (and madly madly) (and red limbed) [hut].
Check and red red.
Check (and red red) hut.
Check red ((and red red) hut).
Check and red limbed.
Fail Check and red limbed john.
Check (and red limbed) [john].
Check madly (and red red) _.
Check john.
Check (and madly madly) (and red limbed).
Check madly (con (_:CON) red limbed).
Check madly (and red limbed).
Check madly red (red _).
Check madly limbed [(and red limbed) [john]].
Fail Check madly limbed (and red limbed (blue [john])).
Check madly limbed (and red limbed [blue [john]]).
Check madly (and red limbed) [john].
Check (and madly madly) (PAST read).
Check (and madly madly) (PAST sleep john).
Check (and madly madly) limbed john.
Check (and madly madly) limbed [red john].
Check (and madly madly) limbed [red hut].
Check (and madly madly) (and red red) john.
Check madly (and red limbed) [hut].
Check (and madly madly) (and red limbed) [john].
Check (and madly madly) (and red limbed) [red john].
Check (and madly madly) (and red limbed) [red [hut]].
Check and_abs blue limbed ([john]: gs _ _ _ _ _).
Fail Check and_abs blue limbed john.
Check and_abs blue limbed [john].
Check and_abs blue red john.
Check and (PAST sleep john) (PAST sleep john).
Check and limbed red.
Check and red red hut.
Fail Check and limbed red john.
Check and limbed red [john].
Check and red limbed [john].
Check (and blue limbed) [john].
Fail Check and blue limbed john.
Check and red red (-s john).
Check red and red red (-s john).
Check red (red and red red (-s john)).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red and limbed red [hut]))).
Check the (red (red (red and limbed red [-s john]))).
Check (and (@red _ _ SG N) (@red _ _ SG N)) hut.
Check (and red red) hut.
Check (and red red) john.
Check (and red red) (-s john).
Check red ((and red red) (-s john)).
Check red (red and red red (-s john)).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red (and limbed red [hut])))).
Check the (red (red (red (and limbed red [-s john])))).
Fail Check a (red (red (red (and limbed red) [-s john]))).
Fail Check (and madly madly) (and red limbed) john.
Check (and madly madly) (and red limbed) [john].
Fail Check red (red (red [and blue limbed john])).
Check red (red (red [and blue limbed [john]])).
Check red (red (red ((and blue red) (-s john)))).
Check a (red (red (red ((and blue red) john)))).
Check a (red (red (red (and limbed red [john])))).
Check red (red ((and blue red) hut)).
Check red (red (red ((and blue red) hut))).
Check red (red (red ((and blue red) john))).
Check red (red (red ((and blue red) (-s hut)))).
Check a (red (red (red ((and blue red) hut)))).
Check a (red (red (red [and blue limbed [hut]]))).
Check a (red (red (red ((and blue red) john)))).
Check red (red (red [and blue limbed [-s john]])).
Check red (red (red (and blue red (-s john)))).
Fail Check a (red (red (red ((and blue red) (-s john))))).
Check a (red (red (red [and blue limbed [john]]))).
Check red (red (red [and blue limbed [-s john]])).
Check (and red red) john.
Check (and red red) (-s john).
Check red (and red red (-s john)).
Check red (red (and red red (-s john))).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red and limbed red [hut]))).
Check the (red (red (red and limbed red [-s john]))).
Check [red].
Check [and blue red].
Check and blue red.
Check red (and blue red hut).
Check red (red and blue red hut).
Check red (red (and blue red hut)).
Check red (red ((and blue red) hut)).
Check red (red [and blue limbed [hut]]).
Check a (red ((and blue red) hut)).
Check a (red (red ((and blue red) hut))).
Check red (red (red ((and blue red) hut))).
Check red (red (red (red ((and blue red) hut)))).
Check red (red ((and blue red) (-s hut))).
Check red (red ((and blue red) (-s hut))).
Fail Check a (red ((and blue red) (-s hut))).
Check a (red ((and blue red) john)).
Check (red ((and blue red) hut)).
Check (and blue red) john.
Check a ((and blue red) john).
Check (and blue red) hut.
Check (and blue red) john.

(* all connectives *)
Check PAST throw john (and (a stone) (-s stone)).
Check at (a ((and blue red) hut)).
Check at (a (and blue red hut)).
Check at (a [(and blue red) hut]).
Check (and blue red) john.
Fail Check [and blue red] hut.
Check (and blue red) hut.
Check and blue red hut.
Check [and blue red hut].
Check [(and blue red) hut].
Check _ _ red hut.
Check and (a stone) (-s stone).
Check and (-s book) (a ball).
Check PAST throw john (and (a stone) (-s stone)).
Check PAST throw john [and john (-s ball)].
Check PAST throw john (and (all (-s john)) (-s ball)).
Check PAST throw john (and john (-s ball)).
Check PAST throw john [and (-s ball) john].
Check PAST throw john (and (-s ball) john).
Check and (-s book) (-s ball).
Check and (-s book) [-s ball].
Fail Check and [-s book] (-s ball). (*per x_base and x_fix, and's 1st arg's type must be gp..*)
Check and ([-s book]: gp _ _ _ _ _) (-s ball).
Check PAST throw john [and (-s book) (-s ball)].
Check PAST throw (every john) (and (-s book) (-s ball)).
Check PAST throw john (and (-s book) [-s ball]).
Check PAST throw john ((and (-s book) ([-s ball]: gp _ Phy _ _ _))).
Fail Check PAST throw john (and [-s book] (-s ball)). (*and's 1st arg's type must be gp..*)
Check PAST throw john (and ([-s book]: gp _ _ _ _ _) (-s ball)).
Check and (a sleep) (-s sleep).
Check and (PAST walk john) (PAST walk john).
Check and (PAST walk john _) (PAST walk john).
Fail Check and (crucially (however (PAST sleep john))) (PAST sleep [-s john]).
Check crucially (however (and (PAST sleep (every john)) (PAST sleep [-s john]))).
Check and (a walk) (-s walk).
Check and (PAST throw john (a ball)) (PAST throw john (-s stone)).
Check and (PAST throw john (a ball) _) (PAST throw john (-s stone)).
Fail Check and (PAST throw john (a ball) _ _) (PAST throw john (-s stone)).
Check and (variad_S0 _ _ _ (PAST throw john (a ball))) (PAST throw john (-s stone)).
Check and (PAST throw john (a stone) (at (the hut))) (PAST throw john (-s stone)).
Fail Check and (PAST throw john (a stone) (at (the hut)))
(PAST throw [red (red (red and blue limbed [-s john]))] (-s stone)).
Check and (PAST throw john (a stone) (at (the hut)))
(PAST throw [red (red (red [and blue limbed [-s john]]))] (-s stone)).
Fail Check and (PAST throw john (a stone) (at (the hut))) [red limbed [-s john]].
Fail Check and (PAST throw john (a stone) (at (the hut))) [red (limbed [-s john])].
Fail Check and (PAST throw john (a stone) (at (the hut))) [red [limbed [-s john]]].

(* l6pp *)