Require Import main.

(*---------------TRUTH-FUNCTIONALITY (OPTIONAL):------------*)

Module UNDec. (* all S-s undecidable *)
Parameter s_prop:> S -> Prop.
End UNDec.

Module TRue. (* all S-s True *)
Coercion s_prop (x:S) := True.
End TRue.

Module NonTriv. (* a nontrivial model *)
Parameter s_prop:> S -> Prop.
Notation ":>> x" := (s_prop (s1s2 (s0s1 x))) (at level 179).
Notation "$ x" := ltac:(let u := constr:(x:Prop) in lazymatch u with
  | :>> (PAST _ _) => exact True
  | s_prop (s1s2 (however (PAST _ _))) => exact True
  | :>> (PRES sleep john) => exact True
  | :>> (variad_S0 _ _ _ (PRES walk (-s boy))) => exact True
  | :>> (PRES _ _) => exact False
  | :>> (variad_S0 _ _ _ (PRES _ _)) => exact False
  | :>> (and (PAST _ _) (PAST _ _)) => exact True (*sleep..*)
  | :>> (and (PAST _ _) (variad_S0 _ _ _ (PAST _ _))) => exact True (*walk..*)
  | :>> (and (PAST _ _) (PRES sleep john)) => exact True
  | :>> (and (PAST _ _) (variad_S0 _ _ _ (PRES walk (-s boy)))) => exact True
  | :>> (and ?m ?n) => idtac "unanalyzed:" m n
  | s_prop (s1s2 (however ?n)) => idtac "unanalyzed:" n
  | x => idtac "unanalyzed:" x": Prop" (*leave some potentially t-functional constructs unanalyzed*)
  end) (at level 199).
End NonTriv.

(*Model switches (comment out at will)*)
(* Import UNDec. (*highest priority if multiple on *) *)
(* Import TRue. *)
Import NonTriv. (*lowest priority if multiple on *)

Check PAST sleep john: Prop.
Check and (PAST sleep john) (PAST walk john): Prop.
Compute PRES sleep john: Prop.
Check $(PRES walk john): Prop. (*False*)
Fail Check $(PAST walk stone). (*type mismatch*)
Check $(PRES sleep john). (*True*)
Check $(PRES sleep (PL boy)). (*False*)
Check $(PAST sleep (-s boy)). (*True*)
Fail Check $sleep. (*type inference fail*)
Fail Check $john. (*type mismatch*)
Fail Check $(PAST _ _). (*type inference fail*)
Check $(and (PAST walk john) (PAST walk john)): Prop. (*True*)
Check $(and (PAST walk john) (PAST sleep john)). (*True*)
Check $(and (PAST sleep john) (PRES sleep john)). (*True*)
Check $(and (PAST sleep john) (PRES walk john)). (*unanalyzed:..*)
Check $(and (PAST sleep john) (PRES walk (-s boy))). (*True*)
Check $(however (PAST sleep john)). (*True*)
Check $(however (PRES sleep john)). (*unanalyzed:..*)
Check $(1=2). (*unanalyzed: (1 = 2): Prop*)
Check $(forall x y:unit, x=y). (*unanalyzed.. Prop*)
Fail Check $Type. (*type mismatch*)

(*a trivial proof that "boys don't sleep" and "john sleeps"*)
Theorem pres: (~ ($ (PRES sleep (-s boy)))) /\ ($ (PRES sleep john)). Proof. firstorder. Qed.
